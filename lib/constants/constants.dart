import 'package:crudwithapi/viewPage/index.dart';
import 'package:flutter/material.dart';

const String APIurlGetUsers = "http://www.sopheamen.com/example/api/users";
const String APIurlEditUser =
    "http://www.sopheamen.com/example/api/user_update/";
const String APIurlPostUsers =
    "http://www.sopheamen.com/example/api/user_store";
const String APIurlDeleteUsers =
    "http://www.sopheamen.com/example/api/user_delete/";

showMessage(BuildContext context, String message) {
  Widget yesButton = FlatButton(
    child: Text("ok"),
    onPressed: () {
      Navigator.pop(context);

      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => IndexPage()),
          (Route<dynamic> route) => false);
    },
  );

  AlertDialog alert = new AlertDialog(
    title: Text("Message"),
    content: Text(message),
    actions: [yesButton],
  );

  showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      });
}
