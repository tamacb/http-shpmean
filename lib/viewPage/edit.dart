import 'dart:convert';
import 'package:crudwithapi/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class EditPage extends StatefulWidget {
  String userId;
  String fullname;
  String email;

  EditPage({this.userId, this.fullname, this.email});

  @override
  _EditPageState createState() => _EditPageState();
}

class _EditPageState extends State<EditPage> {
  final TextEditingController _controllerFullname = new TextEditingController();
  final TextEditingController _controllerEmail = new TextEditingController();
  String userId = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
       userId = widget.userId;
      _controllerFullname.text = widget.fullname;
      _controllerEmail.text = widget.email;
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Page"),
      ),
      body: getBody(),
    );
  }

  Widget getBody() {
    return ListView(
      padding: EdgeInsets.all(30),
      children: [
        SizedBox(
          height: 30,
        ),
        TextField(
          controller: _controllerFullname,
          decoration: InputDecoration(hintText: "Nama Lengkap"),
        ),
        SizedBox(
          height: 30,
        ),
        TextField(
          controller: _controllerEmail,
          decoration: InputDecoration(hintText: "Email"),
        ),
        SizedBox(
          height: 40,
        ),
        FlatButton(
          onPressed: editUser,
          child: Container(
              height: 40,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.deepOrange,
              ),
              child: Center(child: Text("Edit"))),
        )
      ],
    );
  }

  editUser() async {
    var fullname = _controllerFullname.text;
    var email = _controllerEmail.text;
    if(fullname.isNotEmpty && email.isNotEmpty){
      var url = APIurlEditUser+"$userId";
      var bodyData = json.encode({
        'fullname' : fullname,
        'email' : email
      });
      var respon = await http.post(url, headers: {
        "Content-type" : "application/json",
        "Accept" : "application/json"
      }, body: bodyData);
      if(respon.statusCode == 200){
        var messageSucces = json.decode(respon.body)['message'];
        showMessage(context, messageSucces);
      }else{
        var errorMessage = "cant update data!!";
        showMessage(context, errorMessage);
      }
    }

  }
}
