import 'dart:convert';
import 'package:crudwithapi/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CreatePage extends StatefulWidget {
  @override
  _CreatePageState createState() => _CreatePageState();
}

class _CreatePageState extends State<CreatePage> {
  final TextEditingController _controllerFullname = new TextEditingController();
  final TextEditingController _controllerEmail = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create Page"),
      ),
      body: getBody(),
    );
  }

  Widget getBody() {
    return ListView(
      padding: EdgeInsets.all(30),
      children: [
        SizedBox(
          height: 30,
        ),
        TextField(
          controller: _controllerFullname,
          decoration: InputDecoration(hintText: "Nama Lengkap"),
        ),
        SizedBox(
          height: 30,
        ),
        TextField(
          controller: _controllerEmail,
          decoration: InputDecoration(hintText: "Email"),
        ),
        SizedBox(
          height: 40,
        ),
        FlatButton(
          onPressed: createNewUser,
          child: Container(
            height: 40,
            width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.deepOrange,
              ),
              child: Center(child: Text("Add"))),
        )
      ],
    );
  }

  createNewUser() async {
    var fullname = _controllerFullname.text;
    var email = _controllerEmail.text;
    if (fullname.isNotEmpty && email.isNotEmpty){
      var bodyData = json.encode({
        "fullname" : fullname,
        "email" : email
      });
      var respons = await http.post(APIurlPostUsers, headers: {
        "Content-type" : "application/json",
        "Accept" : "aplication/json"
      }, body: bodyData);
      if(respons.statusCode == 200){
        var message = json.decode(respons.body)['message'];
        showMessage(context, message);
        setState(() {
          _controllerFullname.text = "";
          _controllerEmail.text = "";
        });
      }else{
        var errorMessage = "tydac bisa buat user ya";
        showMessage(context, errorMessage);
      }
    }
  }
}
