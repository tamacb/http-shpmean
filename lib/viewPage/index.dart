import 'dart:convert';
import 'package:crudwithapi/constants/constants.dart';
import 'package:crudwithapi/viewPage/create.dart';
import 'package:crudwithapi/viewPage/details.dart';
import 'package:crudwithapi/viewPage/edit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:http/http.dart' as http;

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  List users = [];
  bool isLoad = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.fetchUser();
  }

  fetchUser() async {
    setState(() {
      isLoad = true;
    });
    var response = await http.get(APIurlGetUsers);
    if (response.statusCode == 200) {
      var items = json.decode(response.body)['data'];
      setState(() {
        users = items;
      });
    } else {
      setState(() {
        users = [];
        isLoad = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("CRUD with API"),
      ),
      body: getBody(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => CreatePage()));
        },
      ),
    );
  }

  Widget getBody() {
    return (users.length == 0)
        ? Center(child: CircularProgressIndicator())
        : ListView.builder(
            itemCount: users.length,
            itemBuilder: (context, index) => cardItem(users[index]));
  }

  Widget cardItem(item) {
    var fullname = item['fullname'];
    var email = item['email'];
    return Card(
      child: Slidable(
        actionPane: SlidableDrawerActionPane(),
        actionExtentRatio: 0.25,
        child: Container(
          color: Colors.white,
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.indigoAccent,
              child: Text(item['id'].toString()),
              foregroundColor: Colors.white,
            ),
            title: Text(fullname),
            subtitle: Text(email),
          ),
        ),
        actions: <Widget>[
          IconSlideAction(
            caption: 'Archive',
            color: Colors.blue,
            icon: Icons.archive,
            onTap: () {},
          ),
          IconSlideAction(
            caption: 'Share',
            color: Colors.indigo,
            icon: Icons.share,
            onTap: () => detailUser(item),
          ),
        ],
        secondaryActions: <Widget>[
          IconSlideAction(
            caption: 'Edit',
            color: Colors.black45,
            icon: Icons.edit,
            onTap: () => editUser(item),
          ),
          IconSlideAction(
            caption: 'Delete',
            color: Colors.red,
            icon: Icons.delete,
            onTap: () => showDeleteAlert(context, item),
          ),
        ],
      ),
    );
  }

  detailUser(item) {
    var userId = item['id'].toString();
    var fullname = item['fullname'].toString();
    var email = item['email'].toString();
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => DetailPage(
                  userId: userId,
                  fullname: fullname,
                  email: email,
                )));
  }

  editUser(item) {
    var userId = item['id'].toString();
    var fullname = item['fullname'].toString();
    var email = item['email'].toString();
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => EditPage(
                  userId: userId,
                  fullname: fullname,
                  email: email,
                )));
  }

  deleteUser(userId) async {
    var url = APIurlDeleteUsers + "$userId";
    var respon = await http.post(url, headers: {
      "Content-type": "application/json",
      "Accept": "application/json"
    });
    if (respon.statusCode == 200) {
      this.fetchUser();
    }
  }

  showDeleteAlert(BuildContext context, item) {
    Widget noButton = FlatButton(
      child: Text("No"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    Widget yesButton = FlatButton(
      child: Text("Yes"),
      onPressed: () {
        Navigator.pop(context);
        deleteUser(item['id']);
      },
    );

    AlertDialog alert = new AlertDialog(
      title: Text("Message"),
      content:
          Text("Serius mau ngehapus " + item['fullname'].toString() + " ?"),
      actions: [noButton, yesButton],
    );

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        });
  }
}
